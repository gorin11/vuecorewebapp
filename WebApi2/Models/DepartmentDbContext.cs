﻿using Microsoft.EntityFrameworkCore;

namespace WebApi2.Models
{
	public class DepartmentDbContext : DbContext
	{
		public DepartmentDbContext(DbContextOptions<DepartmentDbContext> options)
			: base(options)
		{
		}

		public DbSet<Department> Departments { get; set; }
	}
}