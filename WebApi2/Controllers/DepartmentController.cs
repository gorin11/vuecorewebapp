﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApi2.Models;

namespace WebApi2.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class DepartmentController : ControllerBase
	{
		public DepartmentController(DepartmentDbContext context)
		{
			DbContext = context;
			if (!DbContext.Departments.Any())
			{
				DbContext.Departments.Add(new Department {Name = "IT department"});
				DbContext.Departments.Add(new Department {Name = "Marketing"});
				DbContext.SaveChanges();
			}
		}

		public DepartmentDbContext DbContext { get; set; }

		[HttpGet]
		public ActionResult<IEnumerable<Department>> Get()
		{
			return DbContext.Departments.ToList();
		}

		[HttpGet("{id}")]
		public ActionResult<string> Get(int id)
		{
			var department = DbContext.Departments.FirstOrDefault(u => u.Id == id);
			if (department == null) return NotFound();
			return new ObjectResult(department);
		}

		[HttpPost]
		public IActionResult Post([FromBody] Department department)
		{
			if (department == null) return BadRequest();

			DbContext.Departments.Add(department);
			DbContext.SaveChanges();
			return Ok(department);
		}

		[HttpPut("{id}")]
		public IActionResult Put(int id, [FromBody] Department department)
		{
			if (department == null) return BadRequest();
			if (!DbContext.Departments.Any(x => x.Id == department.Id)) return NotFound();

			DbContext.Update(department);
			DbContext.SaveChanges();
			return Ok(department);
		}

		// DELETE api/values/5
		[HttpDelete("{id}")]
		public IActionResult Delete(int id)
		{
			var department = DbContext.Departments.FirstOrDefault(x => x.Id == id);
			if (department == null) return NotFound();
			DbContext.Departments.Remove(department);
			DbContext.SaveChanges();
			return Ok(department);
		}
	}
}