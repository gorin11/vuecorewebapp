﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebApi1.DTO;

namespace WebApi1.ExternalData
{
	public interface IWebApi2Data
	{
		Task<Department> GetDepartmentAsync(int id);
	}

	public class WebApi2Data : IWebApi2Data
	{
		static readonly HttpClient client = new HttpClient();
		public async Task<Department> GetDepartmentAsync(int id)
		{
			Department department = null;
			HttpResponseMessage response = await client.GetAsync("https://localhost:5003/api/department/" +id);
			if (response.IsSuccessStatusCode)
			{
				department = await response.Content.ReadAsAsync<Department>();
			}
			return department;
		}
	}
}
