﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApi1.ExternalData;
using WebApi1.Models;

namespace WebApi1.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UserController : ControllerBase
	{
		public UserController(UsersDbContext context, IWebApi2Data webApi2Data)
		{
			DbContext = context;
			if (!DbContext.Users.Any())
			{
				DbContext.Users.Add(new User {Name = "johnny81", DepartmentId = 1});
				DbContext.Users.Add(new User {Name = "tommyLy12", DepartmentId = 2});
				DbContext.Users.Add(new User {Name = "sam12345", DepartmentId = 1});
				DbContext.SaveChanges();
			}
			WebApi2Data = webApi2Data;
		}

		public UsersDbContext DbContext { get; }
		public IWebApi2Data WebApi2Data { get; }

		[HttpGet]
		public ActionResult<IEnumerable<User>> Get()
		{
			return DbContext.Users.ToList();
		}

		[HttpGet("{id}")]
		public ActionResult<User> Get(int id)
		{
			var user = DbContext.Users.FirstOrDefault(u => u.Id == id);
			if (user == null) return NotFound();
			return new ObjectResult(user);
		}

		[HttpPost]
		public IActionResult Post([FromBody] User user)
		{
			if (user == null) return BadRequest("User is null!");
			var department = WebApi2Data.GetDepartmentAsync(user.DepartmentId).Result;
			if (department == null) return BadRequest("No exist department");
			DbContext.Users.Add(user);
			DbContext.SaveChanges();
			return Ok(user);
		}

		[HttpPut]
		public IActionResult Put([FromBody] User user)
		{
			if (user == null) return BadRequest();
			if (!DbContext.Users.Any(x => x.Id == user.Id)) return NotFound();

			DbContext.Update(user);
			DbContext.SaveChanges();
			return Ok(user);
		}

		[HttpDelete("{id}")]
		public IActionResult Delete(int id)
		{
			var user = DbContext.Users.FirstOrDefault(x => x.Id == id);
			if (user == null) return NotFound();
			DbContext.Users.Remove(user);
			DbContext.SaveChanges();
			return Ok(user);
		}
	}
}