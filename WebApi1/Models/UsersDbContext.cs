﻿using Microsoft.EntityFrameworkCore;

namespace WebApi1.Models
{
	public class UsersDbContext : DbContext
	{
		public UsersDbContext(DbContextOptions<UsersDbContext> options)
			: base(options)
		{
		}

		public DbSet<User> Users { get; set; }
	}
}