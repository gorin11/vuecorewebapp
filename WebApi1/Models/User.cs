﻿using Newtonsoft.Json;

namespace WebApi1.Models
{
	public class User
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int DepartmentId { get; set; }
	}
}
