Before run solution, change connection strings in Startup.sc files in WebApi1 and WebApi2 if needed. 
Then execute this commands for WebApi1 and WebApi2.
This operations created databases for this solution.

Add-Migration Initial
Update-Database
After set solution property as Multiple startup projects, run solution.