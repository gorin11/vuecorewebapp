﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vue.CoreWebApp.Models.Departments
{
	public class Department
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
