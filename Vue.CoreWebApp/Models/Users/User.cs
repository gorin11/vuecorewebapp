﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Vue.CoreWebApp.Models.Users
{
	public class User
	{
		[JsonProperty(PropertyName = "Id")]
		public int Id { get; set; }

		[JsonProperty(PropertyName = "Name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "DepartmentId")]
		public int DepartmentId { get; set; }
	}
}
