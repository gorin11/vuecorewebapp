﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Vue.CoreWebApp.ExternalData;
using Vue.CoreWebApp.Models;
using Vue.CoreWebApp.Models.Users;

namespace Vue.CoreWebApp.Controllers
{
	public class UserController : Controller
	{
		public UserController(IWebApiUsers webApiUsers)
		{
			WebApiUsers = webApiUsers;
		}

		private IWebApiUsers WebApiUsers { get; }

		[HttpGet]
		public async Task<JsonResult> GetAll()
		{
			var users = await WebApiUsers.GetAllAsync();
			return Json(users);
		}

		[HttpGet]
		public async Task<JsonResult> Get(int id)
		{
			Models.Users.User user = await WebApiUsers.Get(id);
			return Json(user);
		}

		[HttpPost]
		public async Task<string> Add([FromBody]User user)
		{
			string result = await WebApiUsers.Add(user);
			return result;
		}

		[HttpPut]
		public async Task<string> Update([FromBody] User user)
		{
			string result = await WebApiUsers.Update(user);
			return result;
		}

		[HttpDelete]
		public async Task Delete(int id)
		{
			await WebApiUsers.Delete(id);
		}
	}
}