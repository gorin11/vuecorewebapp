﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Vue.CoreWebApp.ExternalData;
using Vue.CoreWebApp.Models.Departments;

namespace Vue.CoreWebApp.Controllers
{
    public class DepartmentController : Controller
    {
	    public DepartmentController(IWebApiDepartments webApiDepartments)
	    {
		    WebAppDepartments = webApiDepartments;
	    }

	    private IWebApiDepartments WebAppDepartments { get; }

	    [HttpGet] public JsonResult GetAll()
	    {
		    List<Department> departments = WebAppDepartments.GetAllAsync().Result;
		    return Json(departments);
	    }
	}
}