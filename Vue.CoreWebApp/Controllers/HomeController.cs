﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Vue.CoreWebApp.ExternalData;
using Vue.CoreWebApp.Models;
using Vue.CoreWebApp.Models.Departments;

namespace Vue.CoreWebApp.Controllers
{
	public class HomeController : Controller
	{
		public IActionResult Index()
		{
			return View();
		}
	}
}
