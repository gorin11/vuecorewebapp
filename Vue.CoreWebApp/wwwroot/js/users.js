"use strict";

var url = 'http://localhost:5001';
var userSelect = new Vue({
    el: '#userSelect',
    data: {
        users: []
    },
    methods: {
        department: function (id) {
            for (var i = 0; i < departmentSelect.departments.length; ++i) {
                if (departmentSelect.departments[i]["id"] === id) {
                    var value = departmentSelect.departments[i]["name"];
                    break;
                }
            }
            return value;
        },
        selectedUser(user) {
            userInput.userId = user.Id;
            userInput.userName = user.Name;
            departmentSelect.selected = user.DepartmentId;
            console.log(user.Name);
        },
        renderUsers() {
            const self = this;
            axios
                .get(url + '/User/GetAll')
                .then(response => (self.users = response.data));
        }
    },
    mounted() {
        const self = this;
        axios
            .get(url + '/User/GetAll')
            .then(response => (self.users = response.data));
    }
});

var departmentSelect = new Vue({
    el: '#departmentSelect',
    data: {
        departments: null,
        selected: ''
    },
    mounted() {
        axios
            .get(url + '/Department/GetAll')
            .then(response => (this.departments = response["data"]));
    }
});
var userInput = new Vue({
    el: '#userInput',
    data: {
        userId: -1,
        userName: ""
    }
});

var crudBtn = new Vue({
    el: '#crud-btn',
    data: {
            userName: '',
            departmentId: ''
    },

    methods: {
        createUser() {
            const user = JSON.stringify({
                'Name': userInput.userName,
                'DepartmentId': departmentSelect.selected
            });
            axios
                .post(url + "/User/Add", user,
                    {
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                .then(response => {
                    console.log(response);
                    userSelect.renderUsers();
                });
        },
        updateUser() {
            const user = JSON.stringify({
                'Id': userInput.userId,
                'Name': userInput.userName,
                'DepartmentId': departmentSelect.selected
            });
            axios
                .put(url + "/User/Update", user,
                    {
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                .then(response => {
                    console.log(response);
                    userSelect.renderUsers();
                });
        },
        deleteUser() {
            axios
                .delete(url + "/User/Delete/" + userInput.userId)
                .then(response => {
                    console.log(response);
                    userSelect.renderUsers();
                });
        }
    }
});

