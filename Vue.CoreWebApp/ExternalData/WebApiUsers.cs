﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Vue.CoreWebApp.Models.Users;

namespace Vue.CoreWebApp.ExternalData
{
	public interface IWebApiUsers
	{
		Task<List<User>> GetAllAsync();
		Task<User> Get(int id);
		Task<string> Add(User user);
		Task Delete(int id);
		Task<string> Update(User user);

	}

	public class WebApiUsers:IWebApiUsers
	{
		public static string UriUserApi { get; } = "https://localhost:5004/api/user/";

		public async Task<List<User>> GetAllAsync()
		{
			List<User> users = null;
			using (HttpClient client = new HttpClient())
			{
				HttpResponseMessage response = await client.GetAsync(UriUserApi);
				if (response.IsSuccessStatusCode)
				{
					users = await response.Content.ReadAsAsync<List<User>>();
				}
			}

			return users;
		}

		public async Task<User> Get(int id)
		{
			User user = null;
			using (HttpClient client = new HttpClient())
			{
				HttpResponseMessage response = await client.GetAsync(UriUserApi + id);
				if (response.IsSuccessStatusCode)
				{
					user = await response.Content.ReadAsAsync<User>();
				}
			}

			return user;
		}

		public async Task<string> Add(User user)
		{
			var userJson = new JObject { { "Name", user.Name }, { "DepartmentId", user.DepartmentId } };
			using (HttpClient client = new HttpClient())
			{
				var content = new StringContent(userJson.ToString(), Encoding.UTF8, "application/json");
				HttpResponseMessage response = await client.PostAsync(UriUserApi, content);
				return response.StatusCode.ToString();

			}
		}

		public async Task<string> Update(User user)
		{
			var userJson = new JObject { {"Id", user.Id }, { "Name", user.Name }, { "DepartmentId", user.DepartmentId } };
			using (HttpClient client = new HttpClient())
			{
				var content = new StringContent(userJson.ToString(), Encoding.UTF8, "application/json");
				HttpResponseMessage response = await client.PutAsync(UriUserApi, content);
				return response.StatusCode.ToString();

			}
		}

		public async Task Delete(int id)
		{
			using (HttpClient client = new HttpClient())
			{
				await client.DeleteAsync(UriUserApi + id);
			}
		}

	}
}
