﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Vue.CoreWebApp.Models.Departments;

namespace Vue.CoreWebApp.ExternalData
{
	public interface IWebApiDepartments
	{
		Task<List<Department>> GetAllAsync();
	}
	public class WebApiDepartments: IWebApiDepartments
	{
		static readonly HttpClient client = new HttpClient();

		public async Task<List<Department>> GetAllAsync()
		{
			List<Department> departments = null;
			HttpResponseMessage response = await client.GetAsync("https://localhost:5003/api/department");
			if (response.IsSuccessStatusCode)
			{
				departments = await response.Content.ReadAsAsync<List<Department>>();
			}
			return departments;
		}

		public async Task<Department> GetDepartmentAsync(int id)
		{
			Department department = null;
			HttpResponseMessage response = await client.GetAsync("https://localhost:5003/api/department/" + id);
			if (response.IsSuccessStatusCode)
			{
				department = await response.Content.ReadAsAsync<Department>();
			}
			return department;
		}

		//public async Task EditDepartmentAsync(Department depart)
		//{
		//	HttpResponseMessage response = await client.PutAsync("https://localhost:5003/api/department/",)
		//}
	}
}
